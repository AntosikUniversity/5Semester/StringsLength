package ru.antosik.stringlength;

import ru.antosik.stringlength.Classes.Reader;

import java.util.Scanner;

public class Main {

    /**
     * Main point
     *
     * @param args - CLI args (not needed)
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String endWord = "!q";

        Reader StringReader = new Reader(scanner, endWord);

        System.out.println("Enter words below ↓");
        System.out.printf("If you want to stop - enter %s %n", endWord);
        printLessLengthThanAverage(StringReader.read());
    }

    /**
     * Prints string, which length is less than average
     *
     * @param strings - List with strings
     */
    private static void printLessLengthThanAverage(String[] strings) {
        int totalLength = 0, mediumLength;

        for (String string : strings) {
            totalLength += string.length();
        }

        mediumLength = totalLength / strings.length;
        System.out.printf("Medium strings length: %d \n", mediumLength);
        System.out.println("Strings with less, than average, length: ");

        for (String string : strings) {
            int stringL = string.length();
            if (mediumLength > stringL) {
                System.out.printf("%s - %d chars \n", string, stringL);
            }
        }
    }
}
