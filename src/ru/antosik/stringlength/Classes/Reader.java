package ru.antosik.stringlength.Classes;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

/**
 * Reader class
 */
public class Reader {
    private String endWord;
    private Scanner scanner;

    /**
     * Reader class constructor
     *
     * @param scan Scanner
     * @param end  Word, which end reading
     */
    public Reader(Scanner scan, String end) {
        scanner = Objects.requireNonNull(scan, "Scanner must not be null");
        endWord = Objects.requireNonNull(end, "Ending word must not be null");
    }

    /**
     * Reads strings and adds them to list
     */
    public String[] read() {
        List<String> strings = new ArrayList<>();

        String newWord;
        while (!(newWord = scanner.nextLine()).equals(endWord)) {
            strings.add(newWord);
        }

        return strings.toArray(new String[0]);
    }

    /**
     * Getter for end word
     *
     * @return end word
     */
    public String getEndWord() {
        return endWord;
    }

    /**
     * Setter for end word
     *
     * @param end Word, which end reading
     */
    public void setEndWord(String end) {
        endWord = end;
    }
}
